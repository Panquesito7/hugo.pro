#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

export CHROME_PATH
CHROME_PATH="$(command -v chromium-browser)"

# Set up SSH keys
mkdir -p "$HOME/.ssh" "$CI_PROJECT_DIR/artifacts/"
echo "$SSH_PRIVATE_KEY" > "$HOME/.ssh/id_rsa"
chmod 600 "$HOME/.ssh/id_rsa"
cp "$CI_PROJECT_DIR/.gitlab-ci/known_hosts" "$HOME/.ssh/"

if [[ "$CI_COMMIT_REF_NAME" == "production" ]]; then
  deploy="$DEPLOY_DIR_PRODUCTION"
else
  deploy="$DEPLOY_DIR_STAGING"
fi

# Deploy to server
scp -r dist/* hugo@hugo.pro:"$deploy"
