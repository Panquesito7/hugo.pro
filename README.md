# [hugo.pro](https://hugo.pro)

My personal website.

## Technology stack

### Tools

- [Jekyll](https://jekyllrb.com/)
- [Webpack](https://webpack.js.org/)

### Libraries

- [Barba.js](http://barbajs.org/)
- [Bulma](https://bulma.io/)

### Fonts

- [Inter](https://rsms.me/inter/)

## Development

### Dependencies

- [Node.js](https://nodejs.org/) >= 8.0.0
- [Ruby](https://www.ruby-lang.org/) >= 2.4.0
- [Yarn](https://yarnpkg.com/) >= 1.2.0

### Commands

```bash
# Install Node.js and Ruby dependencies
yarn install && bundle install

# Start a development server on http://localhost:5000/
yarn run dev

# Check CSS and JavaScript using Stylelint and JavaScript Standard Style
yarn run lint

# Remove generated files
yarn run clean
```

## License

Copyright © 2017-2020 Hugo Locurcio

- Unless otherwise specified, files in this repository are licensed under
the MIT license; see [LICENSE.md](LICENSE.md) for details.
- Site content and articles are available under
  [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/).
