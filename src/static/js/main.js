// main.js: Main JavaScript file (initializes features)
//
// Copyright © 2017-2020 Hugo Locurcio - MIT License
// See `LICENSE.md` included in the source distribution for details.

import barba from '@barba/core'
import '../sass/style.sass'

document.addEventListener('DOMContentLoaded', () => {
  // Mobile navbar implementation
  // From <http://bulma.io/documentation/components/navbar/>

  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(
    document.querySelectorAll('.navbar-burger'),
    0
  )

  if ($navbarBurgers.length > 0) {
    $navbarBurgers.forEach(($el) => {
      $el.addEventListener('click', () => {
        // Get the target from the "data-target" attribute
        const $target = document.getElementById($el.dataset.target)

        $el.classList.toggle('is-active')
        $target.classList.toggle('is-active')
      })
    })
  }

  barba.init()
})
